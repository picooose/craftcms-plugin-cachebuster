# CacheBuster plugin for Craft CMS 3.x

Bust cached routes on save

## Requirements

This plugin requires Craft CMS 3.0 or later.

## Installation

To install the plugin, follow these instructions.

1. Open your terminal and go to your Craft project:

        cd /path/to/project

2. Then tell Composer to load the plugin:

        composer require apstudio/cachebuster

3. In the Control Panel, go to Settings → Plugins and click the “Install” button for CacheBuster.

## Configuring CacheBuster

Define your rules in Settings:

- Entry: type of the entry saved
- Endpoint: API endpoint that need to be cleared from the cache


Brought to you by [Adrien Picard](https://www.adrienpicard.co.uk)
