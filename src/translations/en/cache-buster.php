<?php
/**
 * CacheBuster plugin for Craft CMS 3.x
 *
 * Bust cached routes on save
 *
 * @link      https://www.adrienpicard.co.uk
 * @copyright Copyright (c) 2018 Adrien Picard
 */

/**
 * CacheBuster en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('cachebuster', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Adrien Picard
 * @package   CacheBuster
 * @since     1.0.0
 */
return [
    'CacheBuster plugin loaded' => 'CacheBuster plugin loaded',
];
