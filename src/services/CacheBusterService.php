<?php
/**
 * CacheBuster plugin for Craft CMS 3.x
 *
 * Bust cached routes on save
 *
 * @link      https://www.adrienpicard.co.uk
 * @copyright Copyright (c) 2018 Adrien Picard
 */

namespace apstudio\cachebuster\services;

use apstudio\cachebuster\CacheBuster;

use Craft;
use craft\base\Component;
use craft\elements\Entry;

/**
 * CacheBusterService Service
 *
 * All of your plugin’s business logic should go in services, including saving data,
 * retrieving data, etc. They provide APIs that your controllers, template variables,
 * and other plugins can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 *
 * @author    Adrien Picard
 * @package   CacheBuster
 * @since     1.0.0
 */
class CacheBusterService extends Component
{
    // Public Methods
    // =========================================================================

    /**
     * Bust cache if there's a matching rule
     *
     * @param EntryModel $entry
     * @param array $endpoints
     */
    public function bust(Entry $entry, $endpoints){
      $handle = $entry->getType()->handle;
      $siteId = Craft::$app->getSites()->getCurrentSite()->id;
      foreach ($endpoints as $key => $endpoint) {
          if($handle === $endpoint['entry']){
            $cacheKey = sprintf('cache:%s:%s', $siteId, $endpoint['endpoint']);
            //Delete cache
            Craft::$app->getCache()->delete($cacheKey);
          }
      }
    }

    /**
     * This function can literally be anything you want, and you can have as many service
     * functions as you want
     *
     * From any other plugin file, call it like this:
     *
     *     CacheBuster::$plugin->cacheBusterService->exampleService()
     *
     * @return mixed
     */
    // public function exampleService()
    // {
    //     $result = 'something';
    //     // Check our Plugin's settings for `someAttribute`
    //     if (CacheBuster::$plugin->getSettings()->someAttribute) {
    //     }

    //     return $result;
    // }
}
